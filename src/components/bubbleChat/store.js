import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        isActive: true,
        chatPosition: {
            left: '16px',
            top: '16px'
        },
        bubblePosition: {
            right: '16px',
            bottom: '16px'
        }
    },
    mutations: {
        toggle(state) {
            state.isActive = !state.isActive
        },
        changeBubblePosition(bubblePosition, state) {
            state.bubblePosition = bubblePosition
        }
    },
    getters: {
        isActive: state => state.isActive,
        bubblePosition: state => state.bubblePosition,
        chatPosition: state => state.chatPosition
    }
})
