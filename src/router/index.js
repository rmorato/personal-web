import Vue from 'vue'
import Router from 'vue-router'
import HelloPage from '../pages/HelloPage'
import CareerPage from '../pages/CareerPage'
import StudiesPage from '../pages/StudiesPage'
import CoursesPage from '../pages/CoursesPage'
import NotFound from '../pages/NotFoundPage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'hello',
      component: HelloPage
    },
    {
        path: '/career',
        name: 'career',
        component: CareerPage
    },
    {
        path: '/studies',
        name: 'studies',
        component: StudiesPage
    },
    {
        path: '/courses',
        name: 'courses',
        component: CoursesPage
    },
    {
        path: '*',
        name: 'notfound',
        component: NotFound
    }
  ]
})