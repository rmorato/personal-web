const courses = [
    {
        enterprise: "Udemy",
        image: "udemy.png",
        date: {
            month: "October",
            year: "2018"
        },
        title: "Vue JS 2 - The Complete Guide (incl. Vue Router & Vuex)",
        hours: "21"
    },
    {
        enterprise: "Udemy",
        image: "udemy.png",
        date: {
            month: "July",
            year: "2018"
        },
        title: "Node JS: Advanced Concepts",
        hours: "16"
    },
    {
        enterprise: "Universidad Politécnica de Madrid",
        image: "upm.png",
        date: {
            month: "July",
            year: "2017"
        },
        title: "Testing",
        hours: "21"
    }
]

export default courses