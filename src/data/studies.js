const studies = [
    {
        center: "Universidad Autónoma de Madrid",
        description: "Computer Science",
        image: "uam.png",
        startDate: {
            month: "September",
            year: "2013"
        },
        endDate: {
            month: "June",
            year: "2018"
        }
    },
    {
        center: "IES Ciudad Escolar",
        description: "Web Development FP",
        image: "madrid.png",
        startDate: {
            month: "September",
            year: "2011"
        },
        endDate: {
            month: "June",
            year: "2013"
        }
    }
]

export default studies