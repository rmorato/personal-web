const jobs = [
    {
        enterprise: "inbenta",
        image: "inbenta.png",
        startDate: {
            month: "September",
            year: "2018"
        },
        endDate: "",
        description: "full-stack developer",
        technologies: [
            {
                description: "PHP",
                logo: "technologies/php.png"
            },
            {
                description: "Slim",
                logo: "technologies/slim.jpg"
            },
            {
                description: "PHPUnit",
                logo: "technologies/phpunit.png"
            },
            {
                description: "MySQL",
                logo: "technologies/mysql.png"
            },
            {
                description: "JavaScript",
                logo: "technologies/javascript.png"
            },
            {
                description: "Vue, Vuex, Vue-router, Axios",
                logo: "technologies/vue.png"
            },
            {
                description: "Docker",
                logo: "technologies/docker.png"
            },
            {
                description: "Git",
                logo: "technologies/git.png"
            },
            {
                description: "Gitlab",
                logo: "technologies/gitlab.png"
            },
            {
                description: "Jira",
                logo: "technologies/jira.png"
            },
            {
                description: "Scrum",
                logo: "technologies/scrum.png"
            }
        ]
    },
    {
        enterprise: "beBee",
        image: "bebee.png",
        startDate: {
            month: "September",
            year: "2017"
        },
        endDate: {
            month: "September",
            year: "2018"
        },
        description: "back-end developer",
        technologies: [
            {
                description: "PHP",
                logo: "technologies/php.png"
            },
            {
                description: "Slim",
                logo: "technologies/slim.jpg"
            },
            {
                description: "PHPUnit",
                logo: "technologies/phpunit.png"
            },
            {
                description: "Redis",
                logo: "technologies/redis.png"
            },
            {
                description: "Kafka",
                logo: "technologies/kafka.png"
            },
            {
                description: "Solr",
                logo: "technologies/solr.png"
            },
            {
                description: "Git",
                logo: "technologies/git.png"
            },
            {
                description: "BitBucket",
                logo: "technologies/bitbucket.png"
            },
            {
                description: "Jira",
                logo: "technologies/jira.png"
            },
            {
                description: "Scrum",
                logo: "technologies/scrum.png"
            }
        ]
    }
]

export default jobs