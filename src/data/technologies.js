const technologies = [
    {
        description: "PHP",
        logo: "technologies/php.png"
    },
    {
        description: "Slim",
        logo: "technologies/slim.jpg"
    },
    {
        description: "PHPUnit",
        logo: "technologies/phpunit.png"
    },
    {
        description: "MySQL",
        logo: "technologies/mysql.png"
    },
    {
        description: "Redis",
        logo: "technologies/redis.png"
    },
    {
        description: "Solr",
        logo: "technologies/solr.png"
    },
    {
        description: "Kafka",
        logo: "technologies/kafka.png"
    },
    {
        description: "JavaScript",
        logo: "technologies/javascript.png"
    },
    {
        description: "Vue, Vuex, Vue-router, Axios",
        logo: "technologies/vue.png"
    },
    {
        description: "Docker",
        logo: "technologies/docker.png"
    },
    {
        description: "Git",
        logo: "technologies/git.png"
    },
    {
        description: "Gitlab",
        logo: "technologies/gitlab.png"
    },
    {
        description: "Jira",
        logo: "technologies/jira.png"
    },
    {
        description: "Scrum",
        logo: "technologies/scrum.png"
    }
]

export default technologies